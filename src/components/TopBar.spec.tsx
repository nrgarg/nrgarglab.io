import { render, fireEvent, wait } from "@testing-library/react"
import React from "react"
import { TestRouter } from "../../test/TestRouter"
import { TopBar } from "./TopBar"
import { expect } from "chai"

describe("<TopBar />", () => {
    describe("click on home icon", () => {
        it("redirects to home page", async () => {
            const { getByRole, getByTestId } = render(<TestRouter><TopBar /></TestRouter>)

            await wait(() => fireEvent.click(getByTestId("go-to-home")))
        })
    })
    describe("click on contact button", () => {
        it("redirects to contact page", async () => {
            const { getByTestId, debug } = render(<TestRouter><TopBar /></TestRouter>)

            await wait(() => fireEvent.click(getByTestId("go-to-contact")))
        })
    })
    describe("click on miniapps button", () => {
        it("redirects to miniapps page", async () => {
            const { getByTestId } = render(<TestRouter><TopBar /></TestRouter>)

            await wait(() => fireEvent.click(getByTestId("go-to-miniapps")))
        })
    })
})
