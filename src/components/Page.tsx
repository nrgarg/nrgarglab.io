import * as React from "react"
import "./Page.scss"

export interface IPageProps {
    children: React.ReactNode
    title: string
}

export const Page: React.FunctionComponent<IPageProps> = (props) => {
    return (
        <div className="Page">
            <h1>{props.title}</h1>
            {props.children}
        </div>
    )
}
