import * as React from "react"
import { Page } from "../components/Page"
import "./Home.scss"

export const Home: React.FunctionComponent<{}> = () => {
    return (
        <Page title="Welcome">
            <p>
                My name is Navneet Garg and I'm a full time software engineer from Toronto, Ontario,
                currently working in Chicago, Illinois.
            </p>
            <p>
                My end user is the developer.
                I try to dedicate my time in ensuring the developer can deliver
                their code as effecitvely as possible with ease.
                Delivering software isn't easy and improving the ways we can get
                code from ones laptop to production is a huge motivator for me.
                Its not just about (perceived) efficiency - user experience has a big impact too.
            </p>
            <p>
                I also really enjoy educating.
                There is no end to the amount of information required in the software genre,
                but I feel its the small (basic) things that trip someone up more often than not.
                Before we can talk about complex software systems - getting comfortable with the everyday tools
                that we use allows us to free up our minds so that we can actually handle complex software.
                Providing mentorship and guidance in the tools that an SE needs is something gives me great pleasure.
            </p>
            <p>
                I built this site for fun. The miniapps section is just a little quirky thing I like adding to when
                I have time as a way to keep my brain occupied.
                If you want to see the code just
                check it out <a href="https://gitlab.com/nrgarg/nrgarg.gitlab.io">here</a>.
            </p>
            <div className="welcome__technology">
                This site was built using the following technologies
                <ul>
                    <li>TypeScript</li>
                    <li>React</li>
                    <li>Material UI</li>
                    <li>Webpack</li>
                    <li>Gitlab Pages</li>
                </ul>
            </div>
        </Page>
    )
}
