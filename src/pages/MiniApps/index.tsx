import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import * as React from "react"
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from "react-router"
import { Page } from "../../components/Page"
import { DefaultApp } from "./DefaultApp"
import { PokemonPasswordCalculatorApp } from "./PokemonPasswordCalculator"

interface IMiniAppRoute {
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>
    name: string
    path: string
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 200,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
)

const miniAppRouteManifest: IMiniAppRoute[] = [
    {
        component: PokemonPasswordCalculatorApp,
        name: "Pokemon Password Calculator",
        path: "pokemon"
    }
]

export const useMiniApps = () => miniAppRouteManifest

export const MiniApps: React.FunctionComponent<{}> = () => {
    const match = useRouteMatch()
    const miniAppManifest = useMiniApps()

    const routeItems = miniAppManifest.map((appRoute) =>
        <Route key={appRoute.name} path={`${match.url}/${appRoute.path}`} component={appRoute.component} exact={true} />
    )

    return (
        <Page title="Mini Apps">
            <Switch>
                <Route path={`${match.url}`} component={DefaultApp} exact={true} />
                {routeItems}
                <Redirect to={`${match.url}`} />
            </Switch>
        </Page>
    )
}
