import * as React from "react"
import { Page } from "../components/Page"

export const Contact: React.FunctionComponent = (props) => {
    const email = "navneetgarg123@gmail.com"
    const linkedIn = "https://linkedin.com/in/navneetgarg123"
    const gitlab = "https://gitlab.com/nrgarg"
    return (
        <Page title="Contact Me">
            <h3>Want to get in touch?</h3>
            <ul>
                <li><a href={`mailto:${email}`}>Email</a></li>
                <li><a href={linkedIn}>Linked In</a></li>
                <li><a href={gitlab}>Gitlab</a></li>
            </ul>
        </Page>
    )
}
