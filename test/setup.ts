import { JSDOM } from "jsdom"

const jsdom = new JSDOM()

const { window } = jsdom

// @ts-ignore
global.window = window

// @ts-ignore
global.navigator = { userAgent: "node.js" }

// load a document into the global scope
// see https://github.com/airbnb/enzyme/blob/master/docs/guides/jsdom.md
function copyProps(src: any, target: any) {
    Object.defineProperties(target, {
        ...Object.getOwnPropertyDescriptors(src),
        ...Object.getOwnPropertyDescriptors(target)
    })
}

copyProps(window, global)

// // @ts-ignore
// global.HTMLElement = window.HTMLElement

// // @ts-ignore
// global.document = window.document

// @ts-ignore
global.window.Date = global.Date
