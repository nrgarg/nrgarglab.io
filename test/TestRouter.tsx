import { createMemoryHistory } from "history"
import React from "react"
import { Router } from "react-router"

const history = createMemoryHistory()

export const TestRouter: React.FunctionComponent<{ children: React.ReactNode[] | React.ReactNode }> = (props) =>
(
    <Router history={history}>
        {props.children}
    </Router>
)
