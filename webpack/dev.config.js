const merge = require("webpack-merge")
const base = require("./base.config")

module.exports = merge(base, {
    // Enable sourcemaps for debugging webpack's output.
    devtool: "inline-source-map",
    mode: "development",
    devServer: {
        host: "localhost",
        port: 3000,
        historyApiFallback: true
    }
})