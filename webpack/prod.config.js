const merge = require("webpack-merge")
const base = require("./base.config")

module.exports = merge(base, {
    // Enable sourcemaps for debugging webpack's output.
    mode: "production",
    devtool: "source-map",
    output: {
        filename: "bundle.[chunkhash].js",
        chunkFilename: "[name].[chunkhash].js"
    },
})