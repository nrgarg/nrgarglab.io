const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")

module.exports = {
    entry: path.join(__dirname, "..", "src/index.tsx"),
    target: "web",
    output: {
        path: path.join(__dirname, "..", "/dist"),
        filename: "bundle.js",
        publicPath: "/"
    },

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json", ".mjs"]
    },

    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            {   
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader"
            },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test:  /\.(eot|svg|ttf|woff|woff2)$/,
                loader: "file-loader"
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: "Navneet Garg",
            description: "My Profile Site",
            template: __dirname + "/index.ejs",
            filename: "index.html",
            inject: true
        }),
        new CopyWebpackPlugin([
            __dirname + "/favicon.ico",
            __dirname + "/CNAME"
        ])
    ]
}